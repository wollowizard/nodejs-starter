FROM node:12-slim
WORKDIR /usr/app
COPY ./dist/ ./dist/
CMD ["node", "./dist/index.js"]
