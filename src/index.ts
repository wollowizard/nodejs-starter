require('dotenv').config()
import * as _ from "lodash";
import express from 'express';

const app = express();
app.get("/", (req: express.Request, res: express.Response) => res.json({"status": "OK"}).end() )
console.log(`Starting service on port ${process.env.PORT}`);

app.listen(process.env.PORT, () =>
  console.log('App listening on port 8080!'),
);
