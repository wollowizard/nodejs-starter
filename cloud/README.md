#Set project
gcloud config set project alfredo-test


#Enable artifact registry (needed only 1st time)
gcloud services enable containerregistry.googleapis.com

#Enable cloudrun
https://console.cloud.google.com/cloud-build/settings/service-account?project=alfredo-test and enable cloud run



#Create build config remote
gcloud builds submit --config cloudbuild.yaml
